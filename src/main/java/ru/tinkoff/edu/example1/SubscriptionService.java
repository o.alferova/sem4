package ru.tinkoff.edu.example1;

import java.util.UUID;

public interface SubscriptionService {

    /**
     * Создать подписку. У подписки есть id и цена
     * @param subscriptionId
     * @param fee
     */
    void createSubscription(UUID subscriptionId, long fee);

    /**
     * Подписать пользователя на подписку
     */
    void subscribeUser(UUID subscriptionId, UUID userId);

    /**
     * Удалить подписку
     */
    void deleteSubscription(UUID subscriptionId);

    /**
     * Посчитать платеж клиенту
     */
    long calculateTotalFee(UUID userId);
}
