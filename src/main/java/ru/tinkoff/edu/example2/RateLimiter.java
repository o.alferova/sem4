package ru.tinkoff.edu.example2;

public class RateLimiter {

    private long tokens;

    public RateLimiter(long initialTokens) {
        this.tokens = initialTokens;
    }

    public boolean doWork() {
        if(tokens-- > 0) {
            return true;
        } else {
            tokens++;
            return false;
        }
    }
}
