package ru.tinkoff.edu.example4;

import org.junit.jupiter.api.RepeatedTest;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LoadingCacheTest {


    //что выполнится
    //что быстрее
    @RepeatedTest(value = 10)
    void getConcurrently() throws ExecutionException, InterruptedException, TimeoutException {
        // given
        AtomicInteger invocationCount = new AtomicInteger();
        Function<String, String> loader = (key) -> {
            invocationCount.incrementAndGet();
            return key.toLowerCase();
        };
         var cache = new LoadingCache<String, String>(loader);

        var executor = Executors.newFixedThreadPool(10);
        // when
        CompletableFuture.allOf(IntStream.range(0, 100_000)
            .mapToObj(i -> CompletableFuture.runAsync(() -> {
                for (int j = 0; j < 100; j++) {
                    cache.get(String.valueOf(i % 1_000));
                }
            }, executor))
            .toArray(CompletableFuture[]::new)).get();
        // then
        assertEquals(1_000, invocationCount.get());
        assertEquals(1_000, cache.getSize());
    }
}
