package ru.tinkoff.edu.example3;

import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertThrows;

class StorageWithBreakStorageWithPeriodicDumpTest {

    @Test
    void dumpWithImproperLocks() throws InterruptedException {
        // given
        var dump = new BreakStorageWithPeriodicDump((map) -> {
            throw new RuntimeException();
        });
        // when
        TimeUnit.MILLISECONDS.sleep(100);
        assertThrows(RuntimeException.class, () -> dump.write("key", "value"));
        // then
        // deadlock here
        var anotherThread = new Thread(() -> {
            dump.read("key");
        });
        anotherThread.start();
        anotherThread.join();
    }

}
