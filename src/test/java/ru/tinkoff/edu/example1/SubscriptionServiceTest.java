package ru.tinkoff.edu.example1;

import org.junit.jupiter.api.Test;

import java.util.UUID;
import java.util.stream.IntStream;

class SubscriptionServiceTest {


    @Test
    void unsubscribe() throws InterruptedException {
        // given
        var userService = new SubscriptionServiceBase();

        long time = System.currentTimeMillis();

        var subscriptionId = UUID.randomUUID();
        var usersIds = IntStream.range(0, 10000).mapToObj(i -> UUID.randomUUID()).toList();
        userService.createSubscription(subscriptionId, 100L);
        usersIds.forEach(userId -> userService.subscribeUser(subscriptionId, userId));
        // when
        var calculateFeeThread = new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                usersIds.forEach(userId -> userService.calculateTotalFee(userId));
            }
        }, "calculateFeeThread");
        calculateFeeThread.start();

        var removeSubscriptionThread = new Thread(() -> {
            userService.deleteSubscription(subscriptionId);
        }, "removeSubscriptionThread");

        removeSubscriptionThread.start();
        // then
        removeSubscriptionThread.join();
        calculateFeeThread.interrupt();
    }

}
